# -*- coding: utf-8 -*-
import ngram
import sys
import pandas


def get_data_from_csv():
    global df1
    global df2
    global df3
    df1 = pandas.read_csv('unigram.csv', index_col=0)
    df2 = pandas.read_csv('bigram.csv', index_col=0)
    df3 = pandas.read_csv('trigram.csv', index_col=0)


ZERO_LOG = -10000000000
ZERO_THRESHOLD = -1000


def get_logprob(df, word):
    return df.loc[word][1] if word in df.index else ZERO_LOG


def calculate_join_prob(words):
    if len(words) == 0:
        return 0
    elif len(words) == 1:
        return get_logprob(df1, words[0])
    elif len(words) == 2:
        return get_logprob(df2, '-'.join(words)) + get_logprob(df1, words[0])
    elif len(words) == 3:
        return get_logprob(df3, '-'.join(words)) + calculate_join_prob(words[:2])
    else:
        return get_logprob(df3, '-'.join(words[-3:])) + calculate_join_prob(words[:-1])


def score(words):
    word_prob = calculate_join_prob(words)
    if word_prob > ZERO_THRESHOLD:
        return (len(words) * len(words)) + (-1.0/word_prob) if len(words) >= 1 else 0
    else:
        return 0

"""
def best_tokens_recursive(words=[], l=0, r=0):
    if l >= r:
        return 0, []
    max_score = 0, [];
    for i in range(l, r-1):
        for j in range(i+1, r):
            templ = best_tokens_recursive(words,l, i)
            tempsc = score(words[i:j]), [i, j]
            tempr = best_tokens_recursive(words,j,r)
            print (i, j, words[i:j], score(words[i:j]))
            if max_score[0] < templ[0] + tempsc[0] + tempr[0]:
                max_score = templ[0] + tempsc[0] + tempr[0], [templ[1], [i, j], tempr[1]]
    return max_score
"""

def best_tokens_dypgram(words=[]):
    n = len(words);
    buffer = [[ [0, []] for i in range(0, n +1) ] for i in range(0, n + 1) ]

    #special case where len = 0 or 1
    for i in range(0, n+1):
        buffer[i][i] = 0, [i, i]
    for i in range(0, n):
        buffer[i][i+1] = score(words[i:i+1]), [i, i+1]

    for words_len in range(2, n+1):#L =
        for i in range(0, n - words_len + 1):
            buffer[i][ i + words_len]  = 0, [];
            for k in range (i, i + words_len ):
                for l in range(k + 1, i + words_len + 1):
                    temp_score = buffer[i][k][0] + score(words[k: l]) + buffer[l][ i + words_len][0]
                    if temp_score > buffer[i][ i + words_len][0]:
                        buffer[i][ i + words_len] = temp_score, [k, l]
    return buffer

def test_calculate_join_prob():
    global df1
    global df2
    global df3

    df1 = pandas.DataFrame(columns=['a', 'b'])
    df2 = pandas.DataFrame(columns=['a', 'b'])
    df3 = pandas.DataFrame(columns=['a', 'b'])

    df1.loc['chưa'] = [1, -1]
    df1.loc['chứng'] = [1, -2]
    df1.loc['minh'] = [1, -3]
    df1.loc['minh'] = [1, -4]
    df1.loc['đủ'] = [1, -5]

    df2.loc['chưa-chứng'] = [1, -10]
    df2.loc['chứng-minh'] = [1, -20]
    df2.loc['minh-đủ'] = [1, -30]

    df3.loc['chưa-chứng-minh'] = [1, -100]
    df3.loc['chứng-minh-đủ'] = [1, -200]

    print (calculate_join_prob(['chưa','chứng','minh', 'đủ']))

"""
def get_print_out(output, result):
    if output == []:
        return
    else:
        if type(output[0]) == int:
            result.append([output[0], output[1]])
            return
        else:
            for item in output:
                get_print_out(item, result)
"""

def get_print_out(words, buffer, l, r, result):
    terms_range = buffer[l][r]
    if terms_range == (0, []) or terms_range[1][0] == terms_range[1][1]:
        return;
    get_print_out(words, buffer, l, terms_range[1][0], result)
    result.append(terms_range)
    get_print_out(words, buffer, terms_range[1][1], r, result)


def tokenize(term):
    words = term.lower().split(' ')
    buffer = best_tokens_dypgram(words=words)
    output = []
    get_print_out(words, buffer, 0, len(words) , output)

    for item in output:
        temp = words[item[1][0]: item[1][1]]
        print ('(')
        for term in temp:
            print (term);
        print (')')

    return output, words, buffer



if __name__ == "__main__":
    sample1 = 'đi thăm nước cộng hòa xã hội chủ nghĩa việt nam'
    sample3 = 'Nhiều ủy viên Bộ Chính trị được giới thiệu tái cử'
    sample4 = 'Phó Ban tuyên Trung ương cho biết ngay sau khi các đoàn kết thúc ghi phiếu ứng cử'
    sample5 = 'Tận tình giúp đỡ anh đồng nghiệp người Nhật được công ty mẹ gửi sang TP HCM'
    sample6 = 'Wikipedia là một bách khoa toàn thư mở bằng nhiều loại ngôn ngữ'
    sample7 = 'Wikipedia là một bách khoa toàn thư'
    sample8 = 'và dự án Wikipedia tiếng Việt được xem như khởi động lại nữa'
    sample9 = 'lần đầu tiên'
    sample10 = 'Chiếc xe ở Virginia bị tuyết bao phủ gần như toàn bộ'
    sample11= 'Wikipedia tiếng Việt không có thêm bài viết nào cho đến tháng 10 năm 2003 khi Trang Chính được viết'
    sample12 = 'Trong hơn 60 người được các đoàn đề cử chiều 24/1 có Chủ tịch nước Trương Tấn Sang'
    sample13 = 'M10 vừa có bàn vô lê đẹp mắt mang tính quyết định giúp Barca giành chiến thắng 2-1 ngay trên sân của Malaga'
    sample15 = 'Những tờ báo lớn ở xứ sương mù có cùng nhận định trận Liverpool thắng Norwich 5-4 là trận đấu điên rồ nhất cho tới thời điểm này của mùa giải'
    get_data_from_csv()
    output, words,  buffer = tokenize(sample5)

    print (output)

