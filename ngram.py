# -*- coding: utf-8 -*-
import codecs
from collections import Counter
import re
import string
import math
import pandas


def create_ngram_key(term):
    words = term.split(' ')
    if len(words) == 0:
        return ''
    else:
        return '-'.join(words)


def remove_empty_sentence(data):
    i = 0
    while i < len(data):
        if data[i] in (u'', '…', '', u' ', u'',  u',', '\n', '\t'):
            del data[i]
        else:
            i += 1

        if (i % 1000 == 0):
            print ('removing empty data for %s sentence(s)' % i)

    return data


def create_sentence_delimiter_regex():
    return '|'.join(map(re.escape, string.punctuation))


def extract_text_from_corpus(data):
    paragraph_list = data.split('\n')
    result = []
    regex = create_sentence_delimiter_regex()
    count = 1
    for paragraph in paragraph_list:
        temp = paragraph.split('\t')
        if len(temp) < 4:
            print (temp)
            continue
        text = temp[4]
        text = text.replace(u'”', '').replace(u'“', '').replace('\"', '').replace('\'', '').lower()
        temp_sentences = re.split(regex, text)
        for i, sentence in enumerate(temp_sentences):
            temp_sentences[i] = sentence.strip()
        result += temp_sentences

        if count % 100 == 0:
            print ('processed %s paragraph(s)' % count)
        count += 1

    print ('removing empty sentences')
    result = remove_empty_sentence(result)
    print ('finished read paragraphs')
    return result


def get_sentences(corpus_name):

    with codecs.open(corpus_name, 'r', encoding='utf-8') as file:
        data = file.read()
        text = extract_text_from_corpus(data)

    i = 0
    while i < len(text):
        if len(text[i]) <= 1:
            del text[i]
        else:
            i += 1

    return text


def create_n_chunk(sentence, n):
    #n>2
    words = sentence.split(' ')
    result = []

    for i in range(0, len(words) - n - 1):
        result += [words[i:i+n]]

    return result;


def update_ngram(ngram,  chunks):

    for chunk in chunks:
        key = '-'.join(chunk)
        ngram[key] += 1 if key in ngram else 1;
        #ngram += Counter([key])



def create_ngram(sentences, n):
    ngram = Counter()
    counter = 1
    for sentence in sentences:
        chunks = create_n_chunk(sentence, n)
        update_ngram(ngram, chunks)
        del chunks
        if counter % 1000 == 0:
            print ('updated %s sentence(s) for %s-gram' %(str(counter), str(n)))
        counter += 1

    return {'data': ngram, 'sum': sum(ngram.values())}


#def create_ngram2():#infuture, will replace create_ngram by less strictly function (no ordering)


def create_unigram(sentences):
    unigram = Counter();
    for sentence in sentences:
        chunks = sentence.split(' ')
        #unigram += Counter(chunks)

        chunk_count = Counter(chunks)
        for key in chunk_count.keys():
            unigram[key] += chunk_count[key] if key in unigram else chunk_count[key]

    return {'data': unigram, 'sum': sum(unigram.values())}


def calculate_prob(terms, ngram):
    key = terms.replace(' ', '-')
    return math.log(float(ngram['data'][key])/ngram['sum'])


def ngram_to_file(ngram, file_name):
    #note! while writing to csv require encoding='utf-8', reading data from csv into dataframe required NO encoding
    #also, do not add u in front of key string constant while accessing pandas index

    #df = pandas.DataFrame(columns=['count', 'logprob'])
    #count = 1
    #for i, key in enumerate(ngram['data']):
    #    prob = calculate_prob(key.replace('-', ' '), ngram)
    #    df.loc[key] = [ngram['data'][key], prob]

    #    if count == 1000:
    #        print ('added %s rows into pandas' % str(count))
    #    count += 1

    #df.to_csv(file_name, encoding='utf-8')

    import csv
    count = 1
    with open(file_name, 'w') as csv_file:
        writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
        writer.writerow(['index', 'count', 'logprob'])
        for i, key in enumerate(ngram['data']):

            prob = calculate_prob(key.replace('-', ' '), ngram)
            count = ngram['data'][key]
            key = key.encode('utf-8')
            writer.writerow([key, count, prob])

            if count == 1000:
                print ('added %s rows into csv' % str(count))
            count += 1


def ngram_from_file(file_name, key):
    df = pandas.read_csv(file_name)
    return df.loc[key]


if __name__ == "__main__":

    test_sample1 = u'chức trách'
    test_sample2 = u'khoa công nghệ thực phẩm'
    test_sample3 = u'nhà sản xuất'
    test_sample4 = u'ông'
    test_sample5 = u'tự chế'


    #corpus_file = 'Vie_Newspapers.txt'
    corpus_file = 'test.txt'
    sentences = get_sentences(corpus_file)

    print ('number of sentence: ', len(sentences))

    gram1 = create_unigram(sentences)
    print ('created unigram')
    ngram_to_file(gram1, 'unigram.csv')
    print ('wrote unigram')

    gram2 = create_ngram(sentences, 2)
    print ('created bigram')
    ngram_to_file(gram2, 'bigram.csv')
    print ('wrote bigram')

    gram3 = create_ngram(sentences, 3)
    print ('created trigram')
    ngram_to_file(gram3, 'trigram.csv')
    print ('wrote trigram')


    pass