
### NGRAM TOKENIZER ###

* This is the Vietnamese tokenizer that use N-gram in combination with Dynamic programming method to tokenize a sentence. The probabilities of consecutive terms in sentence are built up, only the one with max probability is selected.
* Version 0.01